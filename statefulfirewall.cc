/* Benjamin Goh <bgoh@andrew.cmu.edu>
 *
 * statefulipfilter.{cc,hh} -- Stateful IP-packet filter
 *
 */

#include <click/config.h>
#include <click/confparse.hh>
#include "statefulfirewall.hh"


/* Add header files as required*/
CLICK_DECLS

StatefulFirewall::StatefulFirewall()
{

}

StatefulFirewall::~StatefulFirewall()
{

}


/* prints Connection object for debugging purposes */
void Connection::print() const
{
	click_chatter("source ip = %s  dest ip = %s  sport = %d  dport = %d  proto = %d  isfw = %d \n", sourceip.c_str(), destip.c_str(), sourceport, destport, proto, isfw);
} // end print


/* Overlaod == operator to check if two Connection objects are equal. 
 * Return true if equal. false otherwise. I did not include the isfw flag into the comparison.
 * This is because I assume connections are bidirectional, and we are comparing a packet with canonical connections stored in the map. 
 */
bool Connection::operator==(const Connection &other) const
{
	return ((this->sourceip.compare(other.sourceip) == 0) && (this->destip.compare(other.destip) == 0) && (this->sourceport == other.sourceport) && (this->destport == other.destport) && (this->proto == other.proto));
} // end operator==

  
/* Compare two connections to determine the sequence in map. Returns negative if smaller, positive if greater, zero if equal */
int Connection::compare(const Connection other) const
{
	int sip, dip, sport, dport, proto, result;
	bool isfw;

	sip = (this->sourceip.compare(other.sourceip));		
	dip = (this->destip.compare(other.destip));
	sport = (this->sourceport) - (other.sourceport);
	dport = (this->destport) - (other.destport);
	proto = (this->proto) - (other.proto);
	

	if (sip < 0) result = -1;							// this->sourceip is smaller than other.sourceip
	else if (sip > 0) result = 1;						// this->sourceip is greater than other.sourceip
	else if (sip == 0)									// this->sourceip is equal to other.sourceip, thus need to compare destination ip
	{
		if (dip < 0) result = -1;						// this->destip is smaller than other.destip
		else if (dip > 0) result = 1;					// this->destip is greater than other.destip
		else if (dip == 0)								// this->destip is equal to other.destip, thus need to compare source port
		{
			if (sport < 0) result = -1;					// this->sourceport is smaller than other.sourceport
			else if (sport > 0) result = 1;				// this->sourceport is greater than other.sourceport
			else if (sport == 0)						// this->sourceport is equal to other.sourceport, thus need to compare destination port
			{
				if (dport < 0) result = -1;				// this->destport is smaller than other.destport
				else if (dport > 0) result = 1;			// this->destport is greater than other.destport
				else if (dport == 0)					// this->destport is equal to other.destport, thus need to compare proto
				{
					if (proto < 0) result = -1;			// this->proto is smaller than other.proto
					else if (proto > 0) result = 1;		// this->proto is greater than other.proto
					else if (proto == 0) result = 0;	// this->proto is equal to other.proto, thus both connections are equal
				} //end if destport
			} // end if sourceport
		} // end if destip
	} // end if sourceip

	return result; 

} // end compare


/* Return value of isfw*/
bool Connection::is_forward()
{
	return isfw;
} // end is_forward

  
/* Return a Connection object with source ip/port and dest ip/port swapped.
 * This function is used to translate a canonical connection with isfw flag set to false back into its original packet, before comparing with policy.
 */
Connection Connection::swap() const
{
	return Connection(destip, sourceip, destport, sourceport, proto, true);
} // end swap


/* Return a Connection object representing policy */
Connection Policy::getConnection()
{
	return Connection(sourceip, destip, sourceport, destport, proto, true);
} // end getConnection


/* Return action for this Policy */
int Policy::getAction()
{
	return action;
} // end getAction


/* prints Policy object for debugging purposes */
void Policy::print() const
{
	click_chatter("source ip = %s  dest ip = %s  sport = %d  dport = %d  proto = %d  action = %d \n", sourceip.c_str(), destip.c_str(), sourceport, destport, proto, action);
} // end print


/* Take the configuration paramenters as input corresponding to
 * POLICYFILE and DEFAULT where
 * POLICYFILE : Path of policy file
 * DEFAULT : Default action (0/1)
 *
 * Hint: Refer to configure methods in other elemsnts.
 */
int StatefulFirewall::configure(Vector<String> &conf, ErrorHandler *errh)
{
	String filename;		// policy file name
	int value = 0;			// default firewall action, where 0 = DENY and 1 = ACCEPT

	if (cp_va_kparse(conf, this, errh, "POLICYFILE", cpkM, cpFilename, &filename, "DEFAULT", cpkM, cpInteger, &value, cpEnd) < 0)
	{
		return -1;
	}

	DEFAULTACTION = value;			// set DEFAULTACTION to value, where 1 = ALLOW and 0 = DENY
	read_policy_config(filename);	

} // end configure


/* return true if Packet represents a new TCP connection (since TCP is the only connection oriented protocol)
 * Check that connection does not exist in the map and TCP SYN flag is set.
 * Else return false.
 */
bool StatefulFirewall::check_if_new_connection(const Packet *p)
{
	Connection conn = get_canonicalized_connection(p);
	std::map<Connection, int>::const_iterator it;

	it = Connections.find(conn);														// look for connection in map
	if ((it == Connections.end()) && (p->tcp_header()->th_flags & TH_SYN)) return true; // connection not in map and syn flag set 
	else return false;

} // end check_if_new_connection 


/*Check if the packet represent TCP Connection reset
 * Return true if TCP RST or FIN flag is set.
 * Else return false.
 */
bool StatefulFirewall::check_if_connection_reset(const Packet *p)
{
	if ((p->tcp_header()->th_flags & TH_RST) || (p->tcp_header()->th_flags & TH_FIN)) return true;
	else return false;
} // end check_if_connection_reset


/* Add a new connection to the map along with its action.*/
void StatefulFirewall::add_connection(Connection &c, int action)
{
	Connections.insert(std::pair<Connection, int>(c, action));
} // end add_connection


/* Delete the connection from map*/
void StatefulFirewall::delete_connection(Connection &c)
{
	Connections.erase(c);
} // end delete_connection


/* Create a new connection object for Packet.
 * Make sure you canonicalize the source and destination ip address and port number.
 * i.e, make the source less than the destination and
 * update isfw to false if you have to swap source and destination.
 * return NULL on error. 
 */
Connection StatefulFirewall::get_canonicalized_connection(const Packet *p)
{
	String sourceip;
	String destip;
	int sourceport;
	int destport;
	int proto;
	bool isfw;

	const unsigned char *nwheader;	// ptr to start of network header
	char dotted_sip[16];			// source ip in dotted decimal format			
	char dotted_dip[16];			// dest ip in dotted decimal format
	uint32_t sip, dip;				// 32-bit unsigned integer to store the 32-bit source ip and dest ip
	const char *temp_sip;
	const char *temp_dip;

	nwheader = p->network_header();																// pointer to start of network header (ip header)
	proto = nwheader[9];																		// extract protocol field
	sip = (nwheader[12] << 24) | (nwheader[13] << 16) | (nwheader[14] << 8) | (nwheader[15]);	// extract source ip field 
	dip = (nwheader[16] << 24) | (nwheader[17] << 16) | (nwheader[18] << 8) | (nwheader[19]);	// extract destination ip field
	sourceport = (nwheader[20] << 8) | (nwheader[21]);											// extract source port field
	destport = (nwheader[22] << 8) | (nwheader[23]);											// extract destination port field

	dotted_addr(&sip, dotted_sip);	// get source ip in dotted decimal form
	dotted_addr(&dip, dotted_dip);	// get destination ip in dotted decimal form
	temp_sip = dotted_sip;					
	temp_dip = dotted_dip;

	if (sip < dip)		// if source ip is less than destination ip, set isfw to TRUE
	{
		isfw = true;
		sourceip = String(temp_sip);
		destip = String(temp_dip);
	}

	else				// if source ip equals or more than dest ip, swap source ip/port and dest ip/port and set isfw to FALSE
	{
		isfw = false;
		sourceip = String(temp_dip);	// swap source and dest ip
		destip = String(temp_sip);		
		int temp = sourceport;			// swap source and dest ports
		sourceport = destport;
		destport = temp;
	}

	return Connection(sourceip, destip, sourceport, destport, proto, isfw);

} // end get_canonicalized_connection


 /* Read policy from a config file whose path is passed as parameter.
  * Update the policy database.
  * Policy config file structure would be space separated list of
  * <source_ip source_port destination_ip destination_port protocol action>
  * Add Policy objects to the list_of_policies
  */
int StatefulFirewall::read_policy_config(String filename)
{
	const int MAX_CHARS_PER_LINE = 512;
	const int MAX_TOKENS_PER_LINE = 20;
	const char* const DELIMITER = " ";

	string line;						// string of a single line in the policy file
	ifstream f_handle;					// file handle

	String sourceip;
	String destip;
	int sourceport;
	int destport;
	int proto;
	int action;

	f_handle.open(filename.c_str());	// open policy file
	if (!f_handle.good())
		return 1;

	char buf[MAX_CHARS_PER_LINE];
	while (f_handle.getline(buf, MAX_CHARS_PER_LINE)) {

		int n = 0;
		const char* comment = "#";
		char first = buf[0];
		const char* token[MAX_TOKENS_PER_LINE] = {};

		if (strncmp(comment, &first, 1) != 0) {
			token[0] = strtok(buf, DELIMITER);

			if (token[0]) {
				for (n = 1; n < MAX_TOKENS_PER_LINE; n++) {
					token[n] = strtok(0, DELIMITER);
					if (!token[n]) break;
				}

			}
			// extract conection fields from policy line with format <source_ip source_port destination_ip destination_port protocol action>
			sourceip = String(token[0]);	
			sourceport = atoi(token[1]);	
			destip = String(token[2]);
			destport = atoi(token[3]);
			proto = atoi(token[4]);
			action = atoi(token[5]);

			Policy pol(sourceip, destip, sourceport, destport, proto, action);	// create Policy object

			list_of_policies.push_back(pol);									// insert Policy object into the end of the Vector

		} // end if

	} // end while

	return 0;

} // end read_policy_config


/* Convert the integer ip address to string in dotted format */
void StatefulFirewall::dotted_addr(const uint32_t *addr, char *s)
{
	sprintf(s, "%u.%u.%u.%u", (*addr & 0xff000000) >> 24, (*addr & 0x00ff0000) >> 16, (*addr & 0x0000ff00) >> 8, (*addr & 0x000000ff));
} // end dotted_addr


/* 
 * Check if Packet belongs to new connection. If new connection, apply the policy on this packet and add the result to the connection map. Else return the action in map.
 * If Packet indicates connection reset, delete the connection from connection map.
 * Return 1 if packet is allowed to pass
 * Return 0 if packet is to be discarded
 */
int StatefulFirewall::filter_packet(const Packet *p)
{
	int result;

	result = DEFAULTACTION;										// default policy action
	
	Connection conn = get_canonicalized_connection(p);			// return canonicalized Connection object from Packet 

	if (check_if_new_connection(p) == true)						// packet initiates a new connection (i.e. SYN)
	{
		if (!conn.is_forward())									// isfw flag is FALSE, therefore need to swap source ip/port and destination ip/port
		{
			Connection copy = conn.swap();
			for (int i = 0; i < list_of_policies.size(); i++)	// check each policy in policy file
			{
				Connection pol = list_of_policies[i].getConnection();

				if (copy == pol)								// packet matches policy
				{
					result = list_of_policies[i].getAction();
					break;
				}
			} // end for
			add_connection(conn, result);						// add packet and corresponding policy action to map
		} // end if

		else // isfw flag is true
		{
			for (int i = 0; i < list_of_policies.size(); i++)	// check each policy in policy file
			{
				Connection pol = list_of_policies[i].getConnection();
				if (conn == pol)								// packet matches policy
				{
					result = list_of_policies[i].getAction();
					break;
				}
			} // end for
			add_connection(conn, result);						// add packet and corresponding policy action to map
		}  // end else
	} // end check_if_new_connection

	else if (check_if_new_connection(p) == false)				// packet not initiating a new connection
	{
		std::map<Connection, int>::const_iterator it;

		it = Connections.find(conn);							// look for connection in map
		if (it != Connections.end())
		{
			result = it->second;								// return action for the connection if found
		}
		else result = 0;										// connection not found in map
	} // end else if check_if_new_connection

	else if (check_if_connection_reset(p) == true)				// packet initiates termination of connection (i.e. RST or FIN)
	{
		std::map<Connection, int>::const_iterator it;

		it = Connections.find(conn);							// look for connection in map
		if (it != Connections.end())
		{
			result = it->second;								// return action for connection if found

			if (result == 1) 									// allow packet since it is closing an existing connection
				delete_connection(conn);						// delete connection from map
		}
		else result = 0;										// reject packet since no existing connection found in map
	} // end else if check_is_connection_reset

	return result;

} // end filter_packet

  
 
/* Push valid traffic on port 1, 
 * Push discarded traffic on port 0 
 */
void StatefulFirewall::push(int port, Packet *p)
{
	int result;

	result = filter_packet(p);
	if (result == 0) output(0).push(p);
	if (result == 1) output(1).push(p);

} // end push

CLICK_ENDDECLS
EXPORT_ELEMENT(StatefulFirewall)
